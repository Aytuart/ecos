import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { close } from '../store/modal'
import { openSuccess } from '../store/modal'

const useForm = (initialState = {}, onSubmit) => {
	const [data, setData] = useState(initialState)
	const [loading, setLoading] = useState(false)
	const dispatch = useDispatch()

	const handleChange = (e) => {
		setData({ ...data, [e.target.name]: e.target.value })
	}

	const handleSubmit = async (e) => {
		e.preventDefault()
		setLoading(true)
		await fetch('http://admin.ecostroy.brandstudio.kz/api/feedback', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(data),
		})
			.then((resp) => {
				setLoading(false)
				setData({ name: '', phone: '' })
				dispatch(close())
				setTimeout(() => {
					dispatch(openSuccess())
				}, 100)
			})
			.catch((err) => console.log(err))
	}

	return { data, handleChange, handleSubmit, loading }
}

export default useForm
