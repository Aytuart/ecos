import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'

const initialState = {
	data: [],
	status: 'idle',
	error: null,
}

export const fetchCases = createAsyncThunk(
	'cases/fetchCases',
	async (limit) => {
		return await fetch(
			`http://admin.ecostroy.brandstudio.kz/api/portfolio?limit=${
				limit ? limit : ''
			}`
		).then((resp) => resp.json())
	}
)

export const casesSlice = createSlice({
	name: 'cases',
	initialState,
	reducers: {},
	extraReducers(builder) {
		builder
			.addCase(fetchCases.pending, (state, action) => {
				state.status = 'loading'
			})
			.addCase(fetchCases.fulfilled, (state, action) => {
				state.status = 'succeeded'
				state.data = [...action.payload]
			})
			.addCase(fetchCases.rejected, (state, action) => {
				state.status = 'failed'
				state.error = action.error.message
			})
	},
})

export default casesSlice.reducer
