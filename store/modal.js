import { createSlice } from '@reduxjs/toolkit'

const initialState = {
	isOpen: false,
	isSuccessOpen: false,
}

export const modalSlice = createSlice({
	name: 'modal',
	initialState,
	reducers: {
		open: (state) => {
			state.isOpen = true
		},
		close: (state) => {
			state.isOpen = false
		},
		openSuccess: (state) => {
			state.isSuccessOpen = true
		},
		closeSuccess: (state) => {
			state.isSuccessOpen = false
		},
	},
})

export const { open, close, openSuccess, closeSuccess } = modalSlice.actions

export default modalSlice.reducer
