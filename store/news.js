import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'

const initialState = {
	data: [],
	status: 'idle',
	error: null,
}

export const fetchNews = createAsyncThunk('news/fetchNews', async (limit) => {
	return await fetch(
		`http://admin.ecostroy.brandstudio.kz/api/news?per_page=${
			limit ? limit : ''
		}`
	).then((resp) => resp.json())
})

export const newsSlice = createSlice({
	name: 'news',
	initialState,
	reducers: {},
	extraReducers(builder) {
		builder
			.addCase(fetchNews.pending, (state, action) => {
				state.status = 'loading'
			})
			.addCase(fetchNews.fulfilled, (state, action) => {
				state.status = 'succeeded'
				state.data = [...action.payload.data]
			})
			.addCase(fetchNews.rejected, (state, action) => {
				state.status = 'failed'
				state.error = action.error.message
			})
	},
})

export default newsSlice.reducer
