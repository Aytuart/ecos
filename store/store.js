import { configureStore } from '@reduxjs/toolkit'
import menuReducer from './menu'
import modalReducer from './modal'
import settingsReducer from './settings'
import casesReducer from './cases'
import newsReducer from './news'

export const store = configureStore({
	reducer: {
		menu: menuReducer,
		modal: modalReducer,
		settings: settingsReducer,
		cases: casesReducer,
		news: newsReducer,
	},
})
