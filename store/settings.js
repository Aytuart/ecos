import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'

const initialState = {
	data: {},
	status: 'idle',
	error: null,
}

export const fetchSettings = createAsyncThunk(
	'settings/fetchSettings',
	async () => {
		return await fetch(
			'http://admin.ecostroy.brandstudio.kz/api/settings'
		).then((res) => res.json())
	}
)

export const settingsSlice = createSlice({
	name: 'settings',
	initialState,
	reducers: {},
	extraReducers(builder) {
		builder
			.addCase(fetchSettings.pending, (state, action) => {
				state.status = 'loading'
			})
			.addCase(fetchSettings.fulfilled, (state, action) => {
				state.status = 'succeeded'
				state.data = { ...action.payload }
			})
			.addCase(fetchSettings.rejected, (state, action) => {
				state.status = 'failed'
				state.error = action.error.message
			})
	},
})

export default settingsSlice.reducer
