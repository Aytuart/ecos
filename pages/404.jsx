import CallbackForm from '../components/CallbackForm/Form'

export default function Custom404() {
	return (
		<>
			<div className={'error-page'}>
				<h1>Извините, такой страницы не найдено.</h1>
				<span>404</span>
			</div>
			<CallbackForm />
		</>
	)
}
