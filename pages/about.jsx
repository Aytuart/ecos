import CallbackForm from '../components/CallbackForm/Form'
import Clients from '../components/Clients/Clients'

import { Content, Hero } from '../components/About'

const About = () => {
	return (
		<>
			<Hero />
			<Content />
			<Clients />
			<CallbackForm />
		</>
	)
}

export default About
