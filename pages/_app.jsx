import '../styles/common.sass'
import '../styles/variables.sass'
import '../styles/fonts.sass'
import '../styles/basic.sass'

import 'swiper/css'
import 'swiper/css/navigation'
import 'swiper/css/pagination'

import Layout from '../components/Layout'

import { store } from '../store/store'
import { Provider } from 'react-redux'

function MyApp({ Component, pageProps }) {
	return (
		<Provider store={store}>
			<Layout>
				<Component {...pageProps} />
			</Layout>
		</Provider>
	)
}

export default MyApp
