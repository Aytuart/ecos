import { useDispatch, useSelector } from 'react-redux'
import { open } from '../store/modal'

import styles from '../components/Contacts/contacts.module.scss'

const Contacts = ({ settings }) => {
	const dispatch = useDispatch()
	const { phone, email, instagram, whatsapp } = settings
	return (
		<div className={styles.wrapper}>
			<div className={styles.top}>
				<h1 className={styles.title}>
					Контакты <br /> Eco Stroy
				</h1>
				<div className={styles.col}>
					<div>
						<a href={`mailto:${email}`} className={styles.email}>
							{email}
						</a>
						<a href={`tel:${phone}`} className={styles.phone}>
							{phone}
						</a>
						<div className={styles.socials}>
							<a
								href={instagram}
								target="_blank"
								rel="noreferrer"
								className={styles.social}
							>
								<img src="/img/insta-orange.svg" alt="icon" />
							</a>
							<a
								href={`https://wa.me/${whatsapp.replace(
									/\s+/g,
									''
								)}`}
								target="_blank"
								rel="noreferrer"
								className={styles.social}
							>
								<img
									src="/img/whatsapp-orange.svg"
									alt="icon"
								/>
							</a>
						</div>
					</div>
				</div>
				<button className={styles.btn} onClick={() => dispatch(open())}>
					Стать клиентом
				</button>
			</div>
			<div className={styles.map}>
				<iframe
					src="https://yandex.ru/map-widget/v1/?um=constructor%3Abd51d487226ec6ee5a2f38ef1dd783f92706b6d58b7a188db92020dad17ec74a&amp;source=constructor"
					width="100%"
					height="100%"
					frameBorder="0"
				></iframe>
			</div>
			<div className={styles.footer}>
				<p>Eco Stroy 2022 © Все права защищены</p>
				<p>Политика конфиденциальности</p>
			</div>
		</div>
	)
}

export default Contacts

export async function getServerSideProps() {
	const response = await fetch(
		`http://admin.ecostroy.brandstudio.kz/api/settings`
	)
	const settings = await response.json()
	return { props: { settings } }
}
