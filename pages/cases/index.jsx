import Image from 'next/image'
import Link from 'next/link'
import CallbackForm from '../../components/CallbackForm/Form'

import styles from '../../components/Cases/cases.module.scss'

import Arrow from '../../public/img/arrow-right.svg'

import { useState } from 'react'

const CaseItem = ({ name, image, attributes, slug }) => {
	return (
		<div className={styles.item}>
			<div className={styles.img}>
				<Image src={image} layout={'fill'} objectFit="cover" />
			</div>
			<div className={styles.itemInner}>
				<h4 className={styles.heading}>{name}</h4>
				<div className={styles.row}>
					{attributes &&
						attributes.map((item, index) => (
							<div className={styles.col} key={index}>
								<h5 className={styles.sub}>{item.title}</h5>
								<p className={styles.text}>{item.value}</p>
							</div>
						))}
				</div>
				<Link href={`/cases/${slug}`}>
					<a className={styles.link}>
						<span className={styles.icon}>
							<Arrow />
						</span>
						Посмотреть работы
					</a>
				</Link>
			</div>
		</div>
	)
}

const Cases = ({ cases, categories }) => {
	const [filteredCases, setFilteredCases] = useState(cases)
	const [activeCategory, setActiveCategory] = useState('all')

	const filterCases = (categoryName) => {
		if (categoryName === 'all') {
			setFilteredCases(cases)
			setActiveCategory('all')
			return
		}
		const arr = cases.filter((item) => {
			return item.category.name === categoryName
		})
		setFilteredCases(arr)
		setActiveCategory(categoryName)
	}

	return (
		<div className={styles.wrapper}>
			<div className={styles.inner}>
				<h1 className={styles.title}>Кейсы</h1>
				<nav className={styles.nav}>
					<button
						className={`${styles.btn} ${
							activeCategory === 'all' ? styles.btnActive : ''
						}`}
						onClick={() => filterCases('all')}
					>
						Все ({cases.length})
					</button>
					{categories &&
						categories.map((item, index) => (
							<button
								key={index}
								onClick={() => filterCases(item.name)}
								className={`${styles.btn} ${
									activeCategory === item.name
										? styles.btnActive
										: ''
								}`}
							>
								{item.name}
							</button>
						))}
				</nav>
				<div className={styles.grid}>
					{cases &&
						filteredCases.map((item, index) => (
							<CaseItem key={index} {...item} />
						))}
				</div>
			</div>
			<CallbackForm />
		</div>
	)
}

export default Cases

export async function getServerSideProps() {
	const casesResponse = await fetch(
		`http://admin.ecostroy.brandstudio.kz/api/portfolio`
	)
	const cases = await casesResponse.json()
	const categoriesResponse = await fetch(
		`http://admin.ecostroy.brandstudio.kz/api/portfolio/categories`
	)
	const categories = await categoriesResponse.json()

	return { props: { cases, categories } }
}
