import CallbackForm from '../../components/CallbackForm/Form'
import { Hero, Gallery, Nav } from '../../components/CaseInner'

const CaseInner = ({ data }) => {
	return (
		<>
			<Hero {...data} />
			<Gallery images={data.images} />
			<Nav prev={data.prev} next={data.next} />
			<CallbackForm />
		</>
	)
}

export default CaseInner

export async function getServerSideProps(context) {
	const { slug } = context.query

	const response = await fetch(
		`http://admin.ecostroy.brandstudio.kz/api/portfolio/${slug}`
	)
	const data = await response.json()

	return { props: { data } }
}
