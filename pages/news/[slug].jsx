import Image from 'next/image'
import Link from 'next/link'
import CallbackForm from '../../components/CallbackForm/Form'

import styles from '../../components/News/inner.module.scss'

import { useRouter } from 'next/router'

const NewsInner = ({ data: { name, content, image, author, date } }) => {
	const router = useRouter()

	return (
		<div className={styles.wrapper}>
			<div className={styles.inner}>
				<button onClick={() => router.back()} className={styles.back}>
					<span className={styles.backIcon}>
						<Image
							src="/img/arrow-line.svg"
							layout={'fill'}
							objectFit="contain"
							alt={'icon'}
						/>
					</span>
					Все статьи
				</button>
				{name && <h1 className={styles.title}>{name}</h1>}
				{image && <img src={image} alt={name} className={styles.img} />}
				{content && (
					<div
						dangerouslySetInnerHTML={{ __html: content }}
						className={styles.content}
					></div>
				)}
				<div className={styles.row}>
					<div className={styles.author}>
						<img
							src="/img/case.jpg"
							alt="avatar"
							className={styles.avatar}
						/>
						<div>
							{author && (
								<p className={styles.authorName}>{author}</p>
							)}
							{date && <p className={styles.date}>{date}</p>}
						</div>
					</div>
					<div className={styles.socials}>
						<p>Поделиться в соцсетях:</p>
						<div>
							<a href="" className={styles.social}>
								<img src="/img/facebook.svg" alt="icon" />
							</a>
							<a href="" className={styles.social}>
								<img src="/img/twitter.svg" alt="icon" />
							</a>
							<a href="" className={styles.social}>
								<img src="/img/vk.svg" alt="icon" />
							</a>
						</div>
					</div>
				</div>
			</div>
			<CallbackForm />
		</div>
	)
}

export default NewsInner

export async function getServerSideProps(context) {
	const { slug } = context.query

	const response = await fetch(
		`http://admin.ecostroy.brandstudio.kz/api/news/${slug}`
	)
	const data = await response.json()

	return { props: { data } }
}
