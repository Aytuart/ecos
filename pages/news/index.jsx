import CallbackForm from '../../components/CallbackForm/Form'
import NewsItem from '../../components/News/Item/Item'
import styles from '../../components/News/news.module.scss'

import { useState } from 'react'

const News = ({ news, categories }) => {
	const { data } = news

	const [filteredNews, setFilteredNews] = useState(data)
	const [activeCategory, setActiveCategory] = useState('all')

	const filterNews = (categoryName) => {
		console.log(categoryName)
		if (categoryName === 'all') {
			setFilteredNews(data)
			setActiveCategory('all')
			return
		}
		const arr = data.filter((item) => {
			return item.category.name === categoryName
		})
		setFilteredNews(arr)
		setActiveCategory(categoryName)
	}

	return (
		<div className={styles.wrapper}>
			<div className={styles.inner}>
				<h1 className={styles.title}>Блог</h1>
				<nav className={styles.nav}>
					<button
						className={`${styles.btn} ${
							activeCategory === 'all' ? styles.btnActive : ''
						}`}
						onClick={() => filterNews('all')}
					>
						Все ({data.length})
					</button>
					{categories &&
						categories.map((item, index) => (
							<button
								key={index}
								onClick={() => filterNews(item.name)}
								className={`${styles.btn} ${
									activeCategory === item.name
										? styles.btnActive
										: ''
								}`}
							>
								{item.name}
							</button>
						))}
				</nav>
				<div className={styles.grid}>
					{data &&
						filteredNews.map((item, index) => (
							<NewsItem key={index} {...item} />
						))}
				</div>
			</div>
			<CallbackForm />
		</div>
	)
}

export default News

export async function getServerSideProps() {
	const newsResponse = await fetch(
		`http://admin.ecostroy.brandstudio.kz/api/news`
	)
	const news = await newsResponse.json()
	const categoriesResponse = await fetch(
		`http://admin.ecostroy.brandstudio.kz/api/news/categories`
	)
	const categories = await categoriesResponse.json()
	return { props: { news, categories } }
}
