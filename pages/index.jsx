import CallbackForm from '../components/CallbackForm/Form'
import Clients from '../components/Clients/Clients'

import { Hero, Plans, About, Works, Pros, News } from '../components/Home'

export default function Home({ cases, news }) {
	return (
		<>
			<Hero />
			<Pros />
			<Works cases={cases} />
			<About />
			<Plans />
			<Clients />
			<News news={news.data} />
			<CallbackForm />
		</>
	)
}

export async function getServerSideProps() {
	const casesResponse = await fetch(
		`http://admin.ecostroy.brandstudio.kz/api/portfolio?limit=4`
	)
	const cases = await casesResponse.json()
	const newsResponse = await fetch(
		`http://admin.ecostroy.brandstudio.kz/api/news?per_page=3`
	)
	const news = await newsResponse.json()

	return { props: { cases, news } }
}
