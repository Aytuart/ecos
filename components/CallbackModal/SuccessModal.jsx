import React from 'react'
import Image from 'next/image'
import InputMask from 'react-input-mask'

import { useSelector, useDispatch } from 'react-redux'
import { closeSuccess } from '../../store/modal'

import styles from './modal.module.scss'

const SuccessModal = () => {
	const dispatch = useDispatch()
	const isSuccessOpen = useSelector((state) => state.modal.isSuccessOpen)

	return (
		<div className={`${styles.modal} ${isSuccessOpen ? 'show-modal' : ''}`}>
			<div
				className={styles.overlay}
				onClick={() => dispatch(closeSuccess())}
			></div>
			<div className={`${styles.wrapper}`}>
				<button
					className={styles.closeBtn}
					onClick={() => dispatch(closeSuccess())}
				>
					<Image src="/img/close.svg" width={34} height={34} />
				</button>
				<h1 className={styles.title}>Спасибо за заявку</h1>
				<p className={styles.text}>
					Ваша заявка получена, мы свяжемся с Вами в ближайшее время!
				</p>
				<p className={styles.text}>
					Мы гарантируем сохранность Ваших личных данных.
				</p>
				<button
					className={styles.outlineBtn}
					onClick={() => dispatch(closeSuccess())}
				>
					Закрыть
				</button>
			</div>
		</div>
	)
}

export default SuccessModal
