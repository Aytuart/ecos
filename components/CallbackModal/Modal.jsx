import React from 'react'
import Image from 'next/image'
import InputMask from 'react-input-mask'

import { useSelector, useDispatch } from 'react-redux'
import { close } from '../../store/modal'
import useForm from '../../hooks/useForm'

import styles from './modal.module.scss'

const CallbackModal = () => {
	const dispatch = useDispatch()
	const isOpen = useSelector((state) => state.modal.isOpen)

	const { data, handleChange, handleSubmit, loading } = useForm({
		name: '',
		phone: '',
	})
	const { name, phone } = data

	return (
		<div className={`${styles.modal} ${isOpen ? 'show-modal' : ''}`}>
			<div
				className={styles.overlay}
				onClick={() => dispatch(close())}
			></div>
			<div
				className={`${styles.wrapper} ${styles.form} ${
					isOpen ? 'show-modal' : ''
				}`}
			>
				<button
					className={styles.closeBtn}
					onClick={() => dispatch(close())}
				>
					<Image src="/img/close.svg" width={34} height={34} />
				</button>
				<h1 className={styles.title}>Обратная связь</h1>
				<form onSubmit={handleSubmit}>
					<input
						type="text"
						placeholder={'Ваше имя'}
						className={styles.input}
						name="name"
						value={name}
						onChange={handleChange}
						required
					/>
					<InputMask
						type="tel"
						name="phone"
						className={styles.input}
						value={phone}
						onChange={handleChange}
						placeholder="Телефон"
						mask={'+7 (999) 999 99 99'}
						required
					/>
					<button
						className={styles.btn}
						disabled={loading}
						type="submit"
					>
						{loading ? (
							<span className={'spinner'}></span>
						) : (
							'Оставить заявку'
						)}
					</button>
				</form>
				<p className={styles.sub}>
					Нажав на кнопку “Отправить”, вы даете согласие на обработку
					персональных данных
				</p>
			</div>
		</div>
	)
}

export default CallbackModal
