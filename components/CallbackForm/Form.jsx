import InputMask from 'react-input-mask'

import styles from './form.module.scss'

import { useState } from 'react'
import useForm from '../../hooks/useForm'

const CallbackForm = () => {
	const { data, handleChange, handleSubmit, loading } = useForm({
		name: '',
		phone: '',
	})
	const { name, phone } = data
	return (
		<div className={styles.wrapper}>
			<img src="/img/hero-bg.svg" alt="image" className={styles.img} />
			<div className={styles.inner}>
				<h1 className={styles.title}>Обратная связь</h1>
				<form onSubmit={handleSubmit} className={styles.form}>
					<input
						type="text"
						placeholder={'Ваше имя'}
						className={styles.input}
						name="name"
						value={name}
						onChange={handleChange}
						required
					/>
					<InputMask
						type="tel"
						name="phone"
						className={styles.input}
						value={phone}
						onChange={handleChange}
						placeholder="Телефон"
						mask={'+7 (999) 999 99 99'}
						required
					/>
					<button
						className={styles.btn}
						disabled={loading}
						type="submit"
					>
						{loading ? (
							<span className={'spinner'}></span>
						) : (
							'Оставить заявку'
						)}
					</button>
				</form>
			</div>
		</div>
	)
}

export default CallbackForm
