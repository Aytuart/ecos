import Image from 'next/image'
import Link from 'next/link'
import styles from './item.module.scss'

const NewsItem = ({ slug, name, image, date, author, category }) => {
	return (
		<Link href={`/news/${slug}`}>
			<a className={styles.wrapper}>
				<span className={styles.img}>
					{image && (
						<Image src={image} layout="fill" objectFit="cover" />
					)}
				</span>
				<span className={styles.row}>
					<p className={styles.category}># {category.name}</p>
					<p className={styles.date}>{date}</p>
				</span>
				<h5 className={styles.heading}>{name}</h5>
				<span className={styles.author}>
					<span className={styles.avatar}>
						<Image
							src={'/img/case.jpg'}
							layout="fill"
							objectFit={'cover'}
						/>
					</span>
					<p className={styles.name}>{author}</p>
				</span>
			</a>
		</Link>
	)
}

export default NewsItem
