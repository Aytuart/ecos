import Link from 'next/link'

import styles from './footer.module.scss'

import Logo from '../../public/img/Logo.svg'
import Image from 'next/image'

import { useSelector } from 'react-redux'

const Footer = () => {
	const { phone, email, address, instagram, whatsapp } = useSelector(
		(state) => state.settings.data
	)

	return (
		<footer className={styles.wrapper}>
			<div className={styles.header}>
				<Link href="/">
					<a className={styles.logo}>
						<Logo />
					</a>
				</Link>
				<nav className={styles.nav}>
					<Link href="/">
						<a>Главная</a>
					</Link>
					<Link href="/cases">
						<a>Кейсы</a>
					</Link>
					<Link href="/about">
						<a>О нас</a>
					</Link>
					<Link href="/news">
						<a>Блог</a>
					</Link>
					<Link href="/contacts">
						<a>Контакты</a>
					</Link>
				</nav>
			</div>
			<div className={styles.inner}>
				<div className={styles.info}>
					<a href={`tel:${phone}`} className={styles.phone}>
						{phone}
					</a>
					<a href={`mailto:${email}`}>
						<p className={styles.text}>{email}</p>
					</a>
					<p className={styles.text}>{address}</p>
					<div className={styles.socials}>
						<a
							className={styles.social}
							href={instagram}
							target="_blank"
							rel="noreferrer"
						>
							<Image
								src="/img/insta.svg"
								width={18}
								height={18}
							/>
						</a>
						<a
							className={styles.social}
							href={`https://wa.me/${whatsapp?.replace(
								/\s+/g,
								''
							)}`}
							target="_blank"
							rel="noreferrer"
						>
							<Image
								src="/img/whatsapp.svg"
								width={18}
								height={18}
							/>
						</a>
					</div>
				</div>
				<div className={styles.map}>
					<iframe
						src="https://yandex.ru/map-widget/v1/?um=constructor%3Abd51d487226ec6ee5a2f38ef1dd783f92706b6d58b7a188db92020dad17ec74a&amp;source=constructor"
						width="100%"
						height="100%"
						frameBorder="0"
					></iframe>
				</div>
			</div>
			<div className={styles.bot}>
				<p className={styles.sub}>
					Eco Stroy 2022 © Все права защищены
				</p>
				<p className={styles.sub}>Политика конфиденциальности</p>
			</div>
		</footer>
	)
}

export default Footer
