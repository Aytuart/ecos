import Image from 'next/image'
import styles from './clients.module.scss'

import { useDispatch } from 'react-redux'
import { open } from '../../store/modal'

const Clients = () => {
	const dispatch = useDispatch()
	return (
		<div className={styles.wrapper}>
			<div className={styles.left}>
				<h1 className={styles.title}>Нам доверяют</h1>
				<p className={styles.text}>
					Поработали с крупнейшими страховыми компаниями, банками,
					ритейл, автомобильными брендами, государственными проектами,
					eСommerce, СМИ и другими.
				</p>
				<button className={styles.btn} onClick={() => dispatch(open())}>
					Стать клиентом
				</button>
			</div>
			<div className={styles.grid}>
				<div className={styles.img}>
					<Image
						src="/img/partner-logo.png"
						width={100}
						height={100}
					/>
				</div>
				<div className={styles.img}>
					<Image
						src="/img/partner-logo.png"
						width={100}
						height={100}
					/>
				</div>
				<div className={styles.img}>
					<Image
						src="/img/partner-logo.png"
						width={100}
						height={100}
					/>
				</div>
				<div className={styles.img}>
					<Image
						src="/img/partner-logo.png"
						width={100}
						height={100}
					/>
				</div>
				<div className={styles.img}>
					<Image
						src="/img/partner-logo.png"
						width={100}
						height={100}
					/>
				</div>
				<div className={styles.img}>
					<Image
						src="/img/partner-logo.png"
						width={100}
						height={100}
					/>
				</div>
				<div className={styles.img}>
					<Image
						src="/img/partner-logo.png"
						width={100}
						height={100}
					/>
				</div>
				<div className={styles.img}>
					<Image
						src="/img/partner-logo.png"
						width={100}
						height={100}
					/>
				</div>
				<div className={styles.img}>
					<Image
						src="/img/partner-logo.png"
						width={100}
						height={100}
					/>
				</div>
			</div>
		</div>
	)
}

export default Clients
