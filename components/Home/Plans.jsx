import styles from './plans.module.scss'

import { useDispatch } from 'react-redux'
import { open } from '../../store/modal'

const Plans = () => {
	const dispatch = useDispatch()

	return (
		<div className={styles.wrapper}>
			<h1 className={styles.title}>Пакеты</h1>
			<div className={styles.grid}>
				<div className={styles.col}>
					<h5 className={styles.heading}>Пакет &apos;Старт&apos;</h5>
					<div className={styles.price}>
						27 900 ₸ <span>/м2</span>
					</div>
					<ul className={styles.list}>
						<li>Демонтаж</li>
						<li>Монтаж</li>
						<li>Штукатурные работы</li>
						<li>Сантехнические работы</li>
						<li>Электромонтажные работы</li>
						<li>Стяжка</li>
						<li>Потолок</li>
						<li>Шпатлевка</li>
						<li>Плиточные работы</li>
						<li>Малярные работы</li>
						<li>Финишные работы</li>
						<li>Прочие материалы и услуги</li>
					</ul>
					<button
						className={styles.btn}
						onClick={() => dispatch(open())}
					>
						Выбрать
					</button>
				</div>
				<div className={`${styles.col} ${styles.orange}`}>
					<h5 className={styles.heading}>
						Пакет &apos;Современный&apos;
					</h5>
					<div className={styles.price}>
						35 000 ₸ <span>/м2</span>
					</div>
					<ul className={styles.list}>
						<li>Демонтаж</li>
						<li>Монтаж</li>
						<li>Штукатурные работы</li>
						<li>Сантехнические работы</li>
						<li>Электромонтажные работы</li>
						<li>Стяжка</li>
						<li>Потолок</li>
						<li>Шпатлевка</li>
						<li>Плиточные работы</li>
						<li>Малярные работы</li>
						<li>Финишные работы</li>
						<li>Прочие материалы и услуги</li>
					</ul>
					<button
						className={styles.btn}
						onClick={() => dispatch(open())}
					>
						Выбрать
					</button>
				</div>
				<div className={styles.col}>
					<h5 className={styles.heading}>
						Пакет &apos;Карт Бланш&apos;
					</h5>
					<div className={styles.price}>
						44 900 ₸ <span>/м2</span>
					</div>
					<ul className={styles.list}>
						<li>Демонтаж</li>
						<li>Монтаж</li>
						<li>Штукатурные работы</li>
						<li>Сантехнические работы</li>
						<li>Электромонтажные работы</li>
						<li>Стяжка</li>
						<li>Потолок</li>
						<li>Шпатлевка</li>
						<li>Плиточные работы</li>
						<li>Малярные работы</li>
						<li>Финишные работы</li>
						<li>Прочие материалы и услуги</li>
					</ul>
					<button
						className={styles.btn}
						onClick={() => dispatch(open())}
					>
						Выбрать
					</button>
				</div>
			</div>
		</div>
	)
}

export default Plans
