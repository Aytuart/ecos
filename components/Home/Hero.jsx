import Image from 'next/image'
import Marquee from 'react-fast-marquee'

import styles from './hero.module.scss'

const Hero = () => {
	return (
		<div className={styles.wrapper}>
			<img
				src="/img/noise.png"
				alt="background"
				className={styles.noise}
			/>
			<img src="/img/hero-bg.svg" alt="bg" className={styles.bg} />
			<Marquee speed={150} gradient={false}>
				<h1 className={styles.title}>
					Мы делаем ремонт коммерческой недвижимости
				</h1>
			</Marquee>
			<div className={styles.inner}>
				<p className={styles.text}>
					Мы фокусируемся исключительно на коммерческой, промышленной
					и общестенной недвижимости.
				</p>
				<p className={styles.text}>
					Ремонт любой сложности по договору с поэтапной оплатой
				</p>
				<div className={styles.row}>
					<div className={styles.col}>
						<span className={styles.value}>90+</span>
						<p className={styles.sub}>клиентов</p>
					</div>
					<div className={styles.col}>
						<span className={styles.value}>120+</span>
						<p className={styles.sub}>
							Объектов сдано <br /> в эксплуатацию
						</p>
					</div>
					<div className={styles.col}>
						<span className={styles.value}>2 200 м²</span>
						<p className={styles.sub}>Освоено нами c 2019 года</p>
					</div>
				</div>
			</div>
			<button className={styles.down}>
				<span className={styles.downIcon}>
					<Image
						src={'/img/arrow-down.svg'}
						layout="fill"
						objectFit={'contain'}
						alt="icon"
					/>
				</span>
				Посмотреть работы
			</button>
		</div>
	)
}

export default Hero
