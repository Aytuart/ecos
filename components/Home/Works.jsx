import Image from 'next/image'
import Link from 'next/link'

import styles from './works.module.scss'

import Arrow from '../../public/img/arrow-right.svg'

import { useSelector } from 'react-redux'

const Item = ({ id, name, image, attributes }) => {
	return (
		<div className={styles.case}>
			<div className={styles.info}>
				{name && <div className={styles.name}>{name}</div>}
				<div className={styles.grid}>
					{attributes &&
						attributes.map((attr, index) => (
							<div className={styles.col} key={index}>
								<h5 className={styles.heading}>{attr.title}</h5>
								<p className={styles.text}>{attr.value}</p>
							</div>
						))}
				</div>
				<Link href={`/cases/${id}`}>
					<a className={styles.link}>
						<span className={styles.icon}>
							<Arrow />
						</span>
						Посмотреть работы
					</a>
				</Link>
			</div>
			{image && <img src={image} alt="image" className={styles.img} />}
		</div>
	)
}

const Works = ({ cases }) => {
	return (
		<div className={styles.wrapper}>
			<h1 className={styles.title}>Наши работы</h1>
			{cases &&
				cases.map((item, index) => <Item key={index} {...item} />)}
			<Link href={'/cases'}>
				<a className={styles.more}>
					<span>Все работы</span>
					<Image src="/img/arrow-line.svg" width={50} height={13} />
				</a>
			</Link>
		</div>
	)
}

export default Works
