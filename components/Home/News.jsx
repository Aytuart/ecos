import Image from 'next/image'
import Link from 'next/link'

import NewsItem from '../News/Item/Item'

import styles from './news.module.scss'

const News = ({ news }) => {
	return (
		<div className={styles.wrapper}>
			<div className={styles.row}>
				<h1 className={styles.title}>Блог</h1>
				<Link href={'/'}>
					<a className={styles.link}>
						<Image
							src={'/img/arrow-line.svg'}
							width={50}
							height={13}
						/>
					</a>
				</Link>
			</div>
			<div className={styles.grid}>
				{news &&
					news.map((item, index) => (
						<NewsItem key={index} {...item} />
					))}
			</div>
		</div>
	)
}

export default News
