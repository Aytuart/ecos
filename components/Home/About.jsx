import styles from './about.module.scss'

const About = () => {
	return (
		<div className={styles.wrapper}>
			<div className={styles.top}>
				<div className={styles.video_wrp}></div>
				<h4 className={styles.heading}>
					Eco Stroy — поможет воплотить в реальность самые
					нестандартные, дерзкие и смелые проекты Вашего бизнеса.
				</h4>
			</div>
			<div className={styles.mid}>
				<div className={styles.block}>
					<span className={`${styles.value} ${styles.big}`}>165</span>
					<p className={styles.text}>
						специалистов <br /> в штате
					</p>
				</div>
				<p className={styles.text}>
					Ремонт коммерческой недвижимости подразумевает разработку
					дизайна, проведение строительных и отделочных работ в
					банках, ресторанах, ночных клубах, магазинах, отелях.
				</p>
			</div>
			<div className={styles.bot}>
				<div className={styles.col}>
					<span className={styles.value}>90+</span>
					<p className={styles.sub}>клиентов</p>
				</div>
				<div className={styles.col}>
					<span className={styles.value}>120+</span>
					<p className={styles.sub}>
						Объектов сдано <br /> в эксплуатацию
					</p>
				</div>
				<div className={styles.col}>
					<span className={styles.value}>15</span>
					<p className={styles.sub}>
						лет <br /> опыта
					</p>
				</div>
			</div>
		</div>
	)
}

export default About
