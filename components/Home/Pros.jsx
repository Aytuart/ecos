import styles from './pros.module.scss'
import { Autoplay, Pagination } from 'swiper'
import { Swiper, SwiperSlide } from 'swiper/react'

const Pros = () => {
	const pagination = ['Скорость', 'Качество', 'Гарантия']

	return (
		<div className={styles.wrapper}>
			<h1 className={styles.title}>
				Eco Stroy - Мы выполняем качественно и в сроки ремонт офисов,
				ресторанов, магазинов, коммерческих помещений любого назначения
				и сложности.
			</h1>
			<Swiper
				modules={[Pagination, Autoplay]}
				className={styles.slider}
				pagination={{
					el: '.swiper-pagination',
					clickable: true,
					renderBullet: function (index, className) {
						return `<span class='${className} ${styles.bullet}'>
									${pagination[index]}
									<svg viewbox="0 0 20 20">
										<circle cx="10" cy="10" r="8" fill="none" stroke="#B6C2BA" stroke-width="3">
										</circle>
										<circle class="circle" cx="10" cy="10" r="8" fill="none" stroke="#FF7707" stroke-width="3">
										</circle>
									</svg>
								</span>`
					},
				}}
				speed={800}
				loop
				autoplay={{ delay: 3000 }}
			>
				<SwiperSlide className={styles.slide}>
					<h3 className={styles.heading}>Скорость</h3>
					<div className={styles.inner}>
						<h4 className={styles.sub}>Выполняем ремонт в сроки</h4>
						<p className={styles.text}>
							Разделяем философию HADI-циклов, итеративной
							разработки. Тестируем, анализируем, предлагаем
							оптимальные решения. Говорим на языке бизнеса.
							Ориентируемся на пользователя. Предлагаем SLA с
							финансовой ответственностью
						</p>
					</div>
					<span className={styles.count}>1</span>
				</SwiperSlide>
				<SwiperSlide className={styles.slide}>
					<h3 className={styles.heading}>Качество</h3>
					<div className={styles.inner}>
						<h4 className={styles.sub}>Выполняем ремонт в сроки</h4>
						<p className={styles.text}>
							Разделяем философию HADI-циклов, итеративной
							разработки. Тестируем, анализируем, предлагаем
							оптимальные решения. Говорим на языке бизнеса.
							Ориентируемся на пользователя. Предлагаем SLA с
							финансовой ответственностью
						</p>
					</div>
					<span className={styles.count}>2</span>
				</SwiperSlide>
				<SwiperSlide className={styles.slide}>
					<h3 className={styles.heading}>Гарантия</h3>
					<div className={styles.inner}>
						<h4 className={styles.sub}>Выполняем ремонт в сроки</h4>
						<p className={styles.text}>
							Разделяем философию HADI-циклов, итеративной
							разработки. Тестируем, анализируем, предлагаем
							оптимальные решения. Говорим на языке бизнеса.
							Ориентируемся на пользователя. Предлагаем SLA с
							финансовой ответственностью
						</p>
					</div>
					<span className={styles.count}>3</span>
				</SwiperSlide>
			</Swiper>
			<div className={`swiper-pagination ${styles.pagination}`}></div>
		</div>
	)
}

export default Pros
