import Link from 'next/link'

import styles from './header.module.scss'

import Logo from '../../public/img/Logo.svg'
import Burger from '../../public/img/Burger.svg'

import { useRouter } from 'next/router'
import { useDispatch, useSelector } from 'react-redux'
import { open } from '../../store/menu'

const Header = () => {
	const router = useRouter()
	const dispatch = useDispatch()
	const phone = useSelector((state) => state.settings.data.phone)

	return (
		<header
			className={`${styles.wrapper} ${
				router.pathname == '/' ? styles.white : ''
			}`}
		>
			<Link href="/">
				<a className={styles.logo}>
					<Logo />
				</a>
			</Link>
			<a href="" className={styles.phone}>
				{phone}
			</a>
			<div className={styles.spacer}></div>
			<Link href="/cases">
				<a className={styles.text}>Кейсы</a>
			</Link>
			<Link href="/contacts">
				<a className={styles.text}>Контакты</a>
			</Link>
			<button className={styles.burger} onClick={() => dispatch(open())}>
				<Burger />
			</button>
		</header>
	)
}

export default Header
