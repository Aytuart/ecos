import styles from './content.module.scss'

import { Pagination, Navigation } from 'swiper'
import { Swiper, SwiperSlide } from 'swiper/react'
import Image from 'next/image'

const AboutContent = () => {
	return (
		<div className={styles.wrapper}>
			<h3 className={styles.title}>
				Eco Stroy - Мы выполняем качественно и в сроки ремонт офисов,
				ресторанов, магазинов, коммерческих помещений любого назначения
				и сложности.
			</h3>
			<div className={styles.inner}>
				<div className={styles.textWrp}>
					<p className={styles.text}>
						Ремонт коммерческой недвижимости подразумевает
						разработку дизайна, проведение строительных и отделочных
						работ в банках, ресторанах, ночных клубах, магазинах,
						отелях.
					</p>
					<div className={styles.dash}></div>
					<p className={styles.sub}>
						Комплексный подход включает разработку дизайн-проекта в
						соответствии с предназначением объекта. Оригинальный
						дизайн требуется развлекательным заведениям, ночным
						клубам и ресторанам. Хотя современные материалы и
						технологии позволяют сделать интересным и уютным даже
						обычное складское помещение.
					</p>
					<p className={styles.text}>
						При отделке нежилых помещений используются разнообразные
						строительные и отделочные материалы. Но все они должны
						отвечать запросам безопасности. Нельзя использовать
						токсичные и легковоспламеняющиеся материалы. Все
						потенциально опасные деревянные или другие конструкции и
						элементы дизайна пропитываются специальными
						противопожарными составами.
					</p>
				</div>
				<Swiper
					modules={[Pagination, Navigation]}
					className={styles.slider}
					pagination={{
						el: '.swiper-pagination',
						type: 'fraction',
					}}
					navigation={{
						prevEl: '.content-prev',
						nextEl: '.content-next',
					}}
					speed={800}
					loop
					autoplay={{ delay: 3000 }}
				>
					<SwiperSlide>
						<div className={styles.img}>
							<Image
								src="/img/case.jpg"
								layout={'fill'}
								objectFit="cover"
							/>
						</div>
					</SwiperSlide>
					<SwiperSlide>
						<div className={styles.img}>
							<Image
								src="/img/case.jpg"
								layout={'fill'}
								objectFit="cover"
							/>
						</div>
					</SwiperSlide>
					<SwiperSlide>
						<div className={styles.img}>
							<Image
								src="/img/case.jpg"
								layout={'fill'}
								objectFit="cover"
							/>
						</div>
					</SwiperSlide>
				</Swiper>
				<div className={styles.nav}>
					<button className={`content-prev ${styles.prev}`}>
						<Image
							src="/img/arrow-line.svg"
							width={50}
							height={13}
						/>
					</button>
					<div
						className={`swiper-pagination ${styles.pagination}`}
					></div>
					<button className={`content-next ${styles.next}`}>
						<Image
							src="/img/arrow-line.svg"
							width={50}
							height={13}
						/>
					</button>
				</div>
			</div>
		</div>
	)
}

export default AboutContent
