import Image from 'next/image'
import styles from './hero.module.scss'

const Hero = () => {
	return (
		<div className={styles.wrapper}>
			<div className={styles.left}>
				<div className={styles.logo}>
					<Image
						src={'/img/Logo.svg'}
						layout={'fill'}
						objectFit={'contain'}
					/>
				</div>
				<h1 className={styles.title}>
					Мы делаем ремонт коммерческой недвижимости
				</h1>
			</div>
			<div className={styles.right}>
				<div className={styles.videoWrp}></div>
			</div>
		</div>
	)
}

export default Hero
