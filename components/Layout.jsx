import Header from './Header/Header'
import Footer from './Footer/Footer'
import Menu from './Menu/Menu'
import CallbackModal from './CallbackModal/Modal'
import SuccessModal from './CallbackModal/SuccessModal'

import { SwitchTransition, CSSTransition } from 'react-transition-group'
import { useRouter } from 'next/router'
import { useRef } from 'react'
import { useEffect } from 'react'

import { useDispatch } from 'react-redux'

import { fetchSettings } from '../store/settings'

export default function Layout({ children }) {
	const router = useRouter()
	const nodeRef = useRef(null)

	const dispatch = useDispatch()

	useEffect(() => {
		dispatch(fetchSettings())
	}, [])

	return (
		<>
			<SuccessModal />
			<CallbackModal />
			<Header />
			<Menu />
			<SwitchTransition>
				<CSSTransition
					key={router.pathname}
					nodeRef={nodeRef}
					in
					timeout={200}
					classNames="fade"
				>
					<main ref={nodeRef} className="layout">
						{children}
					</main>
				</CSSTransition>
			</SwitchTransition>
			{router.pathname != '/contacts' && <Footer />}
		</>
	)
}
