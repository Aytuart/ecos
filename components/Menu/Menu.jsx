import Image from 'next/image'
import Link from 'next/link'
import styles from './menu.module.scss'

import { useDispatch, useSelector } from 'react-redux'
import { close } from '../../store/menu'
import { open } from '../../store/modal'
import { useRouter } from 'next/router'

const Menu = () => {
	const isOpen = useSelector((state) => state.menu.isOpen)
	const { phone, email, address, instagram, whatsapp } = useSelector(
		(state) => state.settings.data
	)
	const dispatch = useDispatch()
	const router = useRouter()

	return (
		<>
			<div
				onClick={() => dispatch(close())}
				className={`${styles.overlay} ${isOpen ? 'show-overlay' : ''}`}
			></div>
			<div className={`${styles.wrapper} ${isOpen ? 'menu-open' : ''}`}>
				<button
					className={styles.closeBtn}
					onClick={() => dispatch(close())}
				>
					<Image
						src={'/img/close.svg'}
						width={40}
						height={40}
						alt="icon"
					/>
				</button>
				<Image
					src={'/img/Logo.svg'}
					width={154}
					height={31}
					alt="logo"
				/>
				<nav className={styles.nav}>
					<Link href="/">
						<a
							onClick={() => dispatch(close())}
							className={`${
								router.pathname == '/' ? 'active' : ''
							}`}
						>
							Главная
						</a>
					</Link>
					<Link href="/cases">
						<a
							onClick={() => dispatch(close())}
							className={`${
								router.pathname == '/cases' ? 'active' : ''
							}`}
						>
							Кейсы
						</a>
					</Link>
					<Link href="/about">
						<a
							onClick={() => dispatch(close())}
							className={`${
								router.pathname == '/about' ? 'active' : ''
							}`}
						>
							О нас
						</a>
					</Link>
					<Link href="/news">
						<a
							onClick={() => dispatch(close())}
							className={`${
								router.pathname == '/news' ? 'active' : ''
							}`}
						>
							Блог
						</a>
					</Link>
					<Link href="/contacts">
						<a
							onClick={() => dispatch(close())}
							className={`${
								router.pathname == '/contacts' ? 'active' : ''
							}`}
						>
							Контакты
						</a>
					</Link>
					<button
						className={styles.btn}
						onClick={() => dispatch(open())}
					>
						Обратная связь
					</button>
				</nav>
				<a href={`tel:${phone}`}>
					<p className={styles.text}>{phone}</p>
				</a>
				<a href={`mailto:${email}`}>
					<p className={styles.text}>{email}</p>
				</a>
				<p className={styles.text}>{address}</p>
				<div className={styles.socials}>
					<a
						href={instagram}
						target="_blank"
						rel="noreferrer"
						className={styles.social}
					>
						<Image
							src="/img/insta-outline.svg"
							width={40}
							height={40}
						/>
					</a>
					<a
						href={`https://wa.me/${whatsapp?.replace(/\s+/g, '')}`}
						className={styles.social}
					>
						<Image
							src="/img/whatsapp-outline.svg"
							width={40}
							height={40}
						/>
					</a>
				</div>
			</div>
		</>
	)
}

export default Menu
