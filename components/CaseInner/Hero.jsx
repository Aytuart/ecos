import styles from './hero.module.scss'
import Image from 'next/image'

const Hero = ({ name, attributes, content, image }) => {
	return (
		<div className={styles.wrapper}>
			<div className={styles.left}>
				{name && <h1 className={styles.title}>{name}</h1>}
				<div className={styles.grid}>
					{attributes &&
						attributes.map((item, index) => (
							<div key={index}>
								<p className={styles.gridText}>
									{item.title} <span>{item.value}</span>
								</p>
							</div>
						))}
				</div>
				<h3 className={styles.heading}>Выполненные работы</h3>
				{content && (
					<div
						className={styles.text}
						dangerouslySetInnerHTML={{ __html: content }}
					></div>
				)}
				<button className={styles.down}>
					<Image
						src={'/img/arrow-down.svg'}
						layout="fill"
						objectFit={'contain'}
						alt={'arrow'}
					/>
				</button>
			</div>
			{image && (
				<div className={styles.img}>
					<Image
						src={image}
						layout={'fill'}
						objectFit={'cover'}
						alt={'image'}
					/>
				</div>
			)}
		</div>
	)
}

export default Hero
