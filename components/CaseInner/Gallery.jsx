import Image from 'next/image'
import { Pagination, Navigation } from 'swiper'
import { Swiper, SwiperSlide, useSwiper } from 'swiper/react'

import styles from './gallery.module.scss'

const SwiperNav = () => {
	const swiper = useSwiper()

	return (
		<>
			<button
				className={`${styles.galleryPrev}`}
				onClick={() => swiper.slidePrev()}
			>
				<span className={styles.galleryPrevIcon}>
					<Image
						src={'/img/slider-arrow.svg'}
						width={39}
						height={38}
					/>
				</span>
			</button>
			<button
				className={`${styles.galleryNext}`}
				onClick={() => swiper.slideNext()}
			>
				<span className={styles.galleryNextIcon}>
					<Image
						src={'/img/slider-arrow.svg'}
						width={39}
						height={38}
					/>
				</span>
			</button>
		</>
	)
}

const Gallery = ({ images }) => {
	const settings = {
		pagination: {
			el: '.swiper-pagination',
			type: 'fraction',
		},
		navigation: {
			prevEl: '.galleryPrev',
			nextEl: '.galleryNext',
		},
		speed: 800,
		loop: true,
		centeredSlides: true,
		slidesPerView: 'auto',
		breakpoints: {
			1000: {
				spaceBetween: 100,
			},
			320: {
				spaceBetween: 20,
			},
		},
	}
	return (
		<div className={styles.wrapper}>
			<Swiper
				modules={[Pagination, Navigation]}
				className={styles.slider}
				{...settings}
			>
				{images &&
					images.map((image, index) => (
						<SwiperSlide key={index} className={styles.slide}>
							<div className={styles.img}>
								<Image
									src={image}
									layout="fill"
									objectFit={'cover'}
									alt="image"
								/>
							</div>
						</SwiperSlide>
					))}
				<SwiperNav />
			</Swiper>
			<nav className={styles.nav}>
				<button className={`galleryPrev ${styles.navArrow}`}>
					<span className={styles.navPrev}>
						<Image
							src={'/img/arrow-line.svg'}
							width={50}
							height={13}
							alt="icon"
						/>
					</span>
				</button>
				<div className={`swiper-pagination ${styles.pagination}`}></div>
				<button className={`galleryNext ${styles.navArrow}`}>
					<span className={styles.navNext}>
						<Image
							src={'/img/arrow-line.svg'}
							width={50}
							height={13}
							alt="icon"
						/>
					</span>
				</button>
			</nav>
		</div>
	)
}

export default Gallery
