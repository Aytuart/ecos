import Image from 'next/image'
import Link from 'next/link'
import styles from './nav.module.scss'

const Nav = ({ prev, next }) => {
	return (
		<div className={styles.wrapper}>
			<Link href={`/cases/${prev.slug}`}>
				<a className={styles.left}>
					<h5 className={styles.sub}>Предыдущий проект</h5>
					<h3 className={styles.heading}>{prev.name}</h3>
					<div className={styles.icon}>
						<Image
							src={'/img/arrow-double.svg'}
							layout={'fill'}
							objectFit={'contain'}
						/>
					</div>
				</a>
			</Link>
			<Link href={`/cases/${next.slug}`}>
				<a className={styles.right}>
					<h5 className={styles.sub}>Следующий проект</h5>
					<h3 className={styles.heading}>{next.name}</h3>
					<div className={`${styles.icon} ${styles.iconRight}`}>
						<Image
							src={'/img/arrow-double.svg'}
							layout={'fill'}
							objectFit={'contain'}
						/>
					</div>
				</a>
			</Link>
		</div>
	)
}

export default Nav
